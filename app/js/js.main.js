
let data={
  img: '',
  alt: '',
  header: '',
  content: '',
};

$(function () {
  // Owl Carousel
  let mealsCarousel = $("#meals-carousel");
  let feedbackCarousel = $("#feedback-carousel");
  mealsCarousel.owlCarousel({
    items: 3,
    margin: 0,
    loop: true,
    nav: true,
    dots: true,
    navText: ["<i class='fas fa-long-arrow-alt-left carousel-control'></i>", "<i class='fas fa-long-arrow-alt-right carousel-control'></i>"]
  });


  feedbackCarousel.owlCarousel({
    items: 1,
    margin: 0,
    loop: true,
    nav: true,
    dots: false,
    navText: ["<i class='fas fa-long-arrow-alt-left carousel-control'></i>", "<i class='fas fa-long-arrow-alt-right carousel-control'></i>"]
  });

  $('#popUpClose').on('click', function () {
    $('#pop-up-wrapper').toggleClass('hidden');
    $('#pop-up').find('#pop-up__description').empty();
  });

  $('.product-card__button').on('click', function () {
    let card = $(this).closest('.product-card')[0];
    let infoList = Object.assign({} ,data);
    infoList['img']=$($(card).find('.product-card__image')[0]).attr('src');
    infoList['alt']=$($(card).find('.product-card__image')[0]).attr('alt');
    infoList['content']=$($(card).find('.product-card__info')[0]).attr('data-product-desc');
    infoList['header']=($($(card).find('.product-card__name')[0]).text());
    fillPopUp(infoList);
    $('#pop-up-wrapper').toggleClass('hidden');

  })

});

function fillPopUp(data) {
  console.log(data);
  let popUp=$('#pop-up');
  let content=data['content'].split(/\r?\n/);
  $(popUp.find('#pop-up__image')).attr('src', data['img']);
  $(popUp.find('#pop-up__image')).attr('alt', data['alt']);
  $(popUp.find('#pop-up__header')).text(data['header']);
  for (let i=0; i<content.length; i++){
    // console.log(content[i]);
    $(popUp.find('#pop-up__description')).append('<p>'+content[i]+'</p>')
  }
}